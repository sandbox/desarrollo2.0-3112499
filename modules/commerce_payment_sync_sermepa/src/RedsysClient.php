<?php

namespace Drupal\commerce_payment_sync_sermepa;

use CommerceRedsys\Payment\Sermepa;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment_sync\Exception\EmptyRemoteOrderIdException;
use Drupal\commerce_payment_sync_sermepa\Exception\RedsysClientInvalidPaymentGatewayException;
use Drupal\commerce_payment_sync\Exception\WrongOrderException;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use RedsysConsultasPHP\Client\Client;
use RedsysConsultasPHP\Model\Transaction;

/**
 * Class RedsysClient.
 *
 * @todo unify the terminology. Rename to Sermepa to Redsys everywhere.
 *
 * @package commerce_payment_sync_sermepa
 */
class RedsysClient extends Client {

  use StringTranslationTrait;

  /**
   * The sermepa API endpoint.
   */
  const ENDPOINT = '/apl02/services/SerClsWSConsulta';

  /**
   * The array of self instances used by the factory method.
   *
   * @var array
   */
  protected static $instances = [];

  /**
   * The commerce payment gateway entity.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface
   */
  protected $paymentGateway;

  /**
   * RedsysClient constructor.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $paymentGateway
   *   The payment gateway to get the sermepa credentials.
   *
   * @throws \CommerceRedsys\Payment\SermepaException
   */
  protected function __construct(PaymentGatewayInterface $paymentGateway) {
    $this->paymentGateway = $paymentGateway;

    $config = [];
    // TODO: Research a better way to inject the logger.
    // There is a known incompatibility between factories and dependency
    // injection. In this case, injecting the logger in the constructor
    // would represents a design error because the factory method could be
    // potentially called for the same payment gateway but with different
    // loggers while only the logger called the first time would be taken
    // in account.
    if (\Drupal::config('commerce_payment_sync.settings')->get('log_enabled')) {
      $logger = \Drupal::logger('commerce_payment_sync');
      $config['logger'] = $logger;
      $config['message_format'] = '{request}';
    }

    $payment_gateway_settings = $paymentGateway->getPluginConfiguration();
    parent::__construct($this->getEndpointAbsoluteUrl(), $payment_gateway_settings['merchant_password'], $config);
  }

  /**
   * Factory method to get instance of class for a specific payment method.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $paymentGateway
   *   The commerce payment gateway entity.
   *
   * @return RedsysClient
   *   The proper RedsysClient instance for the giv en payment gateway.
   *
   * @throws \CommerceRedsys\Payment\SermepaException
   * @throws \Drupal\commerce_payment_sync_sermepa\Exception\RedsysClientInvalidPaymentGatewayException
   */
  public static function getInstance(PaymentGatewayInterface $paymentGateway) {
    $payment_gateway_valid_ids = \Drupal::service('commerce_payment_sync.manager')
      ->getPaymentGatewayIds();
    if (!in_array($paymentGateway->id(), $payment_gateway_valid_ids)) {
      throw new RedsysClientInvalidPaymentGatewayException();
    }
    $id = $paymentGateway->id();
    if (empty(self::$instances[$id])) {
      self::$instances[$id] = new static($paymentGateway);
    }
    return self::$instances[$id];
  }

  /**
   * Get the endpoint absolute url, based on the payment gateway configuration.
   *
   * @return string
   *   The sermepa URL.
   *
   * @throws \CommerceRedsys\Payment\SermepaException
   */
  protected function getEndpointAbsoluteUrl() {
    $payment_gateway_plugin = $this->paymentGateway->getPlugin();
    $gateway_settings = $payment_gateway_plugin->getConfiguration();

    // Create a new instance of the Sermepa library and initialize it.
    $gateway = new Sermepa($gateway_settings['merchant_name'], $gateway_settings['merchant_code'], $gateway_settings['merchant_terminal'], $gateway_settings['merchant_password'], $payment_gateway_plugin->getMode());
    $sermepa_url = $gateway->getEnvironment();

    // Return the API url.
    $url = parse_url($sermepa_url);
    $url['path'] = static::ENDPOINT;
    return $url['scheme']
      . '://' . $url['host']
      . ':' . $url['port']
      . static::ENDPOINT;
  }

  /**
   * Get a redsys transaction from a commerce order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The Order to check.
   *
   * @return \RedsysConsultasPHP\Model\Transaction|null
   *   The redsys transaction object.
   *
   * @throws \Drupal\commerce_payment_sync\Exception\EmptyRemoteOrderIdException
   * @throws \Drupal\commerce_payment_sync\Exception\WrongOrderException
   * @throws \RedsysConsultasPHP\Client\RedsysException
   */
  public function getCommerceOrderTransaction(OrderInterface $order) :?Transaction {
    $payment_remote_id = $order->getData('payment_remote_id');

    if (!$order->hasField('payment_gateway') || $order->get('payment_gateway')->getString() !== $this->paymentGateway->id()) {
      throw new WrongOrderException($this->t('The order provided is not has no :payment_gateway selected as payment gateway', [':payment_gateway' => $this->paymentGateway->id()]));
    }

    if (empty($payment_remote_id)) {
      // @TODO: Fix the message once the referenced commerce_sermepa issue gets fixed.
      throw new EmptyRemoteOrderIdException($this->t('Trying to query Sermepa API on an order without sermepa_order_id. Are you sure this order has raised the sermepa payment gateway? If so, ensure Commerce Sermepa module has the following patch: https://www.drupal.org/project/commerce_sermepa/issues/3113033'));
    }

    $payment_gateway_settings = $this->paymentGateway->getPluginConfiguration();

    return $this->getTransaction(
      $payment_remote_id,
      $payment_gateway_settings['merchant_terminal'],
      $payment_gateway_settings['merchant_code']
    );
  }

}
