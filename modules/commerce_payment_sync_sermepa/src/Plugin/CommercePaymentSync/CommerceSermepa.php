<?php

namespace Drupal\commerce_payment_sync_sermepa\Plugin\CommercePaymentSync;

use CommerceGuys\Intl\Exception\UnknownCurrencyException;
use CommerceRedsys\Payment\Sermepa;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment_sync\CommercePaymentSyncManager;
use Drupal\commerce_payment_sync\CommercePaymentSyncPluginBase;
use Drupal\commerce_payment_sync\Exception\TransactionNotFoundException;
use Drupal\commerce_payment_sync_sermepa\RedsysClient;
use Drupal\commerce_price\Price;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use RedsysConsultasPHP\Client\RedsysException;
use RedsysConsultasPHP\Model\Transaction;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the commerce_payment_sync.
 *
 * @CommercePaymentSync(
 *   id = "commerce_sermepa",
 *   label = @Translation("Foo"),
 *   description = @Translation("Foo description.")
 * )
 */
class CommerceSermepa extends CommercePaymentSyncPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The commerce currecy storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $currencyStorage;

  /**
   * The commerce payment sync manager.
   *
   * @var \Drupal\commerce_payment_sync\CommercePaymentSyncManager
   */
  protected $commercePaymentSyncManager;

  /**
   * CommerceSermepa constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_payment_sync\CommercePaymentSyncManager $commercePaymentSyncManager
   *   The Commerce Payment Sync Manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CommercePaymentSyncManager $commercePaymentSyncManager, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->commercePaymentSyncManager = $commercePaymentSyncManager;
    $this->currencyStorage = $entityTypeManager->getStorage('commerce_currency');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_payment_sync.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteTransaction(OrderInterface $order): ?object {
    $payment_gateway = $this->commercePaymentSyncManager->getPaymentGatewayFromOrder($order);

    $client = RedsysClient::getInstance($payment_gateway);
    try {
      return $client->getCommerceOrderTransaction($order);
    }
    catch (RedsysException $e) {
      switch ($e->getErrorCode()) {
        case 'XML0013':
          // El elemento Ds_Order viene vacío en el XML-String recibido.
        case 'XML0014':
          // El elemento Ds_Order tiene una longitud incorrecta en el XML-String
          // recibido.
        case 'XML0024':
          // No existen operaciones para los datos solicitados.
          throw new TransactionNotFoundException();
      }
      // If the error code is not one of the defined above, throw the exception.
      throw $e;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isTransactionPaid($remoteTransaction): bool {
    /** @var \RedsysConsultasPHP\Model\Transaction $ds_response */
    return $remoteTransaction instanceof Transaction
      && !is_null($remoteTransaction->getDsResponse())
      && (int) $remoteTransaction->getDsResponse() <= Sermepa::MAXIMUN_AUTHORIZATION_CODE_VALUE;
  }

  /**
   * {@inheritdoc}
   */
  public function updatePayment(PaymentInterface $payment, OrderInterface $order, $remoteTransaction): void {

    // Calculate the amount supporting multiple currencies.
    $currencies = $this->currencyStorage
      ->loadByProperties(['numericCode' => $remoteTransaction->getDsCurrency()]);
    if (empty($currencies)) {
      throw new UnknownCurrencyException();
    }
    /** @var \Drupal\commerce_price\Entity\Currency $currency */
    $currency = current($currencies);
    $digits = $currency->getFractionDigits();
    $number = $remoteTransaction->getDsAmount() / pow(10, $digits);
    $amount = new Price($number, $currency->getCurrencyCode());

    $ds_response = $remoteTransaction->getDsResponse();

    // Set all the payment data.
    $payment->setState($this->getPaymentState($remoteTransaction->getDsState()));
    $payment->setAmount($amount);
    $payment->setRemoteId($remoteTransaction->getDsOrder());
    $payment->setRemoteState(Sermepa::handleResponse($ds_response));
    $payment->setAuthorizedTime(strtotime($remoteTransaction->getDsDate() . 'T' . $remoteTransaction->getDsHour()));
  }

  /**
   * {@inheritdoc}
   */
  public function getStatusMapping(): array {
    // @todo This method is mostly duplicated of
    // \Drupal\commerce_sermepa\Plugin\Commerce\PaymentGateway\Sermepa::getStatusMapping()
    // Propose to commerce_sermepa to make the method public and call it form
    // there?
    /* @see \CommerceRedsys\Payment\Sermepa::getAvailableTransactionTypes */
    /* @see commerce/modules/payment/commerce_payment.workflows.yml */
    return [
      // Sermepa/Redsýs: Authorization.
      '0' => 'completed',
      // Sermepa/Redsýs: Pre-authorization.
      '1' => 'authorization',
      // Sermepa/Redsýs: Confirmation of preauthorization.
      '2' => 'authorization',
      // Sermepa/Redsýs: Automatic return.
      '3' => 'refunded',
      // Sermepa/Redsýs: Recurring transaction.
      '5' => 'completed',
      // Sermepa/Redsýs: Successive transaction.
      '6' => 'completed',
      // Sermepa/Redsýs: Pre-authentication.
      '7' => 'authorization',
      // Sermepa/Redsýs: Confirmation of pre-authentication.
      '8' => 'authorization',
      // Sermepa/Redsýs: Annulment of preauthorization.
      '9' => 'authorization_expired',
      // Sermepa/Redsýs: Authorization delayed.
      'O' => 'authorization',
      // Sermepa/Redsýs: Confirmation of authorization in deferred.
      'P' => 'authorization',
      // Sermepa/Redsýs: Delayed authorization Rescission.
      'Q' => 'authorization',
      // Sermepa/Redsýs: Initial recurring deferred released.
      'R' => 'completed',

      // TODO document better the following states. It had been observed that
      // once a order get placed in sermepa, before entering the credit card, a
      // transaction is generated with the S state, but in commerce_sermepa
      // module it is mapped as "completed".
      // Sermepa/Redsýs: Successively recurring deferred released.
      'S' => 'authorization',
      // Sermepa/Redsýs: Successively recurring deferred released.
      'F' => 'completed',
    ];
  }

}
