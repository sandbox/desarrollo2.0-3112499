<?php

namespace Drupal\commerce_payment_sync_sermepa\Exception;

/**
 * Class RedsysClientInvalidPaymentGatewayException.
 *
 * @package commerce_payment_sync\Exception
 */
class RedsysClientInvalidPaymentGatewayException extends \Exception {

}
