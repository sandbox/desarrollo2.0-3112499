<?php

namespace Drupal\commerce_payment_sync_paypal\Plugin\CommercePaymentSync;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment_sync\CommercePaymentSyncManager;
use Drupal\commerce_payment_sync\CommercePaymentSyncManagerInterface;
use Drupal\commerce_payment_sync\CommercePaymentSyncPluginBase;
use Drupal\commerce_payment_sync\Exception\EmptyRemoteOrderIdException;
use Drupal\commerce_payment_sync\Exception\TransactionNotFoundException;
use Drupal\commerce_paypal\CheckoutSdkFactory;
use Drupal\commerce_price\Price;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the commerce_payment_sync.
 *
 * @CommercePaymentSync(
 *   id = "paypal_checkout",
 *   label = @Translation("Commerce paypal"),
 *   description = @Translation("Commerce paypal payment synchronization plugin.")
 * )
 */
class PaypalCheckout extends CommercePaymentSyncPluginBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * The Paypal checkout sdk factory service.
   *
   * @var \Drupal\commerce_paypal\CheckoutSdkFactory
   */
  protected $checkoutSdkFactory;

  /**
   * The commerce payment sync manager.
   *
   * @var \Drupal\commerce_payment_sync\CommercePaymentSyncManagerInterface
   */
  protected $commercePaymentSyncManager;

  /**
   * Paypal constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_payment_sync\CommercePaymentSyncManagerInterface $commercePaymentSyncManager
   *   Commerce payment sync manager.
   * @param \Drupal\commerce_paypal\CheckoutSdkFactory $checkoutSdkFactory
   *   The sdk factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CommercePaymentSyncManagerInterface $commercePaymentSyncManager, CheckoutSdkFactory $checkoutSdkFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->checkoutSdkFactory = $checkoutSdkFactory;
    $this->commercePaymentSyncManager = $commercePaymentSyncManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_payment_sync.manager'),
      $container->get('commerce_paypal.checkout_sdk_factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteTransaction(OrderInterface $order): ?object {
    $payment_remote_id = $order->getData('paypal_order_id');

    if (empty($payment_remote_id)) {
      throw new EmptyRemoteOrderIdException($this->t('Trying to query Paypal API on an order without payment_remote_id. Are you sure this order has raised the paypal payment gateway?'));
    }

    // Instance the paypal sdk based on the payment gateway configuration.
    $payment_gateway = $this->commercePaymentSyncManager->getPaymentGatewayFromOrder($order);
    $checkoutSdk = $this->checkoutSdkFactory->get($payment_gateway->getPluginConfiguration());

    try {
      $response = $checkoutSdk->getOrder($payment_remote_id);
    }
    catch (ClientException $e) {
      switch ((int) $e->getCode()) {
        case 404:
          throw new TransactionNotFoundException();

        default:
          throw $e;

      }
    }
    return (object) Json::decode($response->getBody()->getContents());
  }

  /**
   * {@inheritdoc}
   */
  public function isTransactionPaid($remoteTransaction): bool {
    return !is_null($remoteTransaction) && isset($remoteTransaction->status) && $this->getPaymentState($remoteTransaction->status) === 'completed';
  }

  /**
   * {@inheritdoc}
   */
  public function updatePayment(PaymentInterface $payment, OrderInterface $order, $remoteTransaction): void {
    if (!empty($remoteTransaction->purchase_units)) {
      $number = 0.00;
      foreach ($remoteTransaction->purchase_units as &$purchase_unit) {
        $number += (float) $purchase_unit['amount']['value'];
        // We assume all the purchase_units were made with the same currency.
        $currency_code = $purchase_unit['amount']['currency_code'];
      }
      // Create the amount object.
      $amount = new Price((string) $number, $currency_code);

      // Fill the payment values.
      $payment->setState($this->getPaymentState($remoteTransaction->status));
      $payment->setAmount($amount);
      $payment->setRemoteId($remoteTransaction->id);
      $payment->setRemoteState($remoteTransaction->status);
      $payment->setAuthorizedTime(strtotime($remoteTransaction->create_time));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getStatusMapping(): array {
    /* @see https://developer.paypal.com/docs/api/orders/v2/#orders_get */
    return [
      'CREATED' => 'authorization',
      'SAVED' => 'authorization',
      'APPROVED' => 'authorization',
      'VOIDED' => 'canceled',
      'COMPLETED' => 'completed',
    ];
  }

}
