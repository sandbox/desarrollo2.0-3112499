<?php

namespace Drupal\commerce_payment_sync\Exception;

/**
 * Exception thrown when trying to sync an incorrect order.
 *
 * An order is incorrect when:
 *   - Is canceled.
 *   - Is incorrect.
 *
 * @package commerce_payment_sync\Exception
 */
class WrongOrderException extends \Exception {

}
