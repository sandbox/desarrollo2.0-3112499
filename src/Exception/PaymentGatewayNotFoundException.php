<?php

namespace Drupal\commerce_payment_sync\Exception;

/**
 * Class PaymentGatewayNotFoundException.
 *
 * @package commerce_payment_sync\Exception
 */
class PaymentGatewayNotFoundException extends \Exception {

}
