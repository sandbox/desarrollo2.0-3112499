<?php

namespace Drupal\commerce_payment_sync\Exception;

/**
 * Class EmptySermepaOrderIdException.
 *
 * @package commerce_payment_sync\Exception
 */
class EmptyRemoteOrderIdException extends \Exception {

}
