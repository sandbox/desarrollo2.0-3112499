<?php

namespace Drupal\commerce_payment_sync\Exception;

/**
 * Class TransactionNotCompletedException.
 *
 * @package commerce_payment_sync\Exception
 */
class TransactionNotCompletedException extends \Exception {

}
