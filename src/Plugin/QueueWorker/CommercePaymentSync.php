<?php

namespace Drupal\commerce_payment_sync\Plugin\QueueWorker;

use Drupal\commerce_payment_sync\CommercePaymentSyncManagerInterface;
use Drupal\commerce_payment_sync\Exception\TransactionNotCompletedException;
use Drupal\commerce_payment_sync\Exception\TransactionNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines 'commerce_payment_sync' queue worker.
 *
 * @QueueWorker(
 *   id = "commerce_payment_sync",
 *   title = @Translation("Commerce Payment Sync"),
 *   cron = {"time" = 60}
 * )
 */
class CommercePaymentSync extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The commerce order storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $orderStorage;

  /**
   * The commerce payment sync manager service.
   *
   * @var \Drupal\commerce_payment_sync\CommercePaymentSyncManagerInterface
   */
  protected $commercePaymentSyncManager;

  /**
   * The queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queue;

  /**
   * CommerceSermepaPaymentSync constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\commerce_payment_sync\CommercePaymentSyncManagerInterface $commercePaymentSyncManager
   *   The commerce payment sync manager service.
   * @param \Drupal\Core\Queue\QueueFactory $queue
   *   The queue factory service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, CommercePaymentSyncManagerInterface $commercePaymentSyncManager, QueueFactory $queue) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->orderStorage = $entity_type_manager->getStorage('commerce_order');
    $this->commercePaymentSyncManager = $commercePaymentSyncManager;
    $this->queue = $queue;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('commerce_payment_sync.manager'),
      $container->get('queue')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (!is_array($data) || empty($data['order_id']) || !is_numeric($data['order_id'])) {
      // TODO throw an exception?
      return;
    }
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->orderStorage->load($data['order_id']);

    try {
      $this->commercePaymentSyncManager->processOrder($order);
    }
    catch (TransactionNotCompletedException $e) {
      $this->commercePaymentSyncManager->cancelOrder($order);
    }
    catch (TransactionNotFoundException $e) {
      if (!$this->requeue($data)) {
        $this->commercePaymentSyncManager->cancelOrder($order);
      }
    }
    catch (\Exception $e) {
      watchdog_exception('commerce_payment_sync', $e);
    }
  }

  /**
   * Requeue item.
   *
   * @param array $data
   *   Data item.
   *
   * @return bool
   *   True if the data was requeued, false otherwise.
   */
  public function requeue(array $data): bool {
    if (isset($data['retries']) && $data['retries'] > 0) {
      $data['retries'] -= 1;
      $queue = $this->queue->get($this->getPluginId());
      $queue->createItem($data);
      return TRUE;
    }
    return FALSE;
  }

}
