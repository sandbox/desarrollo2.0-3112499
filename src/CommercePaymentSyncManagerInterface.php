<?php

namespace Drupal\commerce_payment_sync;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;

/**
 * Interface CommerceSermepaPaymentSyncManager.
 *
 * @package commerce_payment_sync
 */
interface CommercePaymentSyncManagerInterface {

  /**
   * Get the pending orders.
   *
   * @return array
   *   The array of pending order entity ids.
   */
  public function getPendingOrderIds(): array;

  /**
   * Process an order checking it against the payment gateway API.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to check.
   */
  public function processOrder(OrderInterface $order): void;

  /**
   * Check if an order is considered as pending.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to check.
   *
   * @return bool
   *   If the order is pending or not.
   */
  public function isOrderPending(OrderInterface $order): bool;

  /**
   * Flag an order as canceled.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   */
  public function cancelOrder(OrderInterface $order): void;

  /**
   * Get the payment gateway entity from a given order entity.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentGatewayInterface
   *   The payment gateway entity.
   *
   * @throws \Drupal\commerce_payment_sync\Exception\PaymentGatewayNotFoundException
   * @throws \Drupal\commerce_payment_sync\Exception\WrongOrderException
   */
  public function getPaymentGatewayFromOrder(OrderInterface $order): PaymentGatewayInterface;

}
