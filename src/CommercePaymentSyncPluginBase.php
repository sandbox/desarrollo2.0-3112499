<?php

namespace Drupal\commerce_payment_sync;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for commerce_payment_sync plugins.
 */
abstract class CommercePaymentSyncPluginBase extends PluginBase implements CommercePaymentSyncInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentState(string $remote_status): ?string {
    $mapping = $this->getStatusMapping();
    if (isset($mapping[$remote_status])) {
      return $mapping[$remote_status];
    }
    return NULL;
  }

}
