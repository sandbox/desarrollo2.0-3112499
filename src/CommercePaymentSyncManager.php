<?php

namespace Drupal\commerce_payment_sync;

use Drupal\commerce_payment_sync\Exception\TransactionNotCompletedException;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment_sync\Exception\PaymentGatewayNotFoundException;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment_sync\Exception\TransactionNotFoundException;
use Drupal\commerce_payment_sync\Exception\WrongOrderException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class CommerceSermepaPaymentSyncManager.
 *
 * @package commerce_payment_sync
 */
class CommercePaymentSyncManager implements CommercePaymentSyncManagerInterface {

  use StringTranslationTrait;

  const PENDING_STATES = [
    'draft',
  ];

  const CHECKOUT_STEPS = [
    'payment',
    'order_information',
  ];

  /**
   * The commerce_payment_sync configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The orders storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $ordersStorage;

  /**
   * The payment gateway storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $paymentGatewayStorage;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The payment storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $paymentStorage;

  /**
   * The commerce_payment_sync plugin manager.
   *
   * @var \Drupal\commerce_payment_sync\CommercePaymentSyncPluginManager
   */
  protected $pluginManager;

  /**
   * The lock backend service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * CommerceSermepaPaymentSyncManager constructor.
   *
   * @param \Drupal\commerce_payment_sync\CommercePaymentSyncPluginManager $pluginManager
   *   The commerce_payment_sync plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *   The lock backend service.
   *
   * @throws PluginNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(CommercePaymentSyncPluginManager $pluginManager, ConfigFactoryInterface $config, EntityTypeManagerInterface $entityTypeManager, LockBackendInterface $lock) {
    $this->pluginManager = $pluginManager;
    $this->config = $config->get('commerce_payment_sync.settings');
    $this->ordersStorage = $entityTypeManager->getStorage('commerce_order');
    $this->paymentGatewayStorage = $entityTypeManager->getStorage('commerce_payment_gateway');
    $this->paymentStorage = $entityTypeManager->getStorage('commerce_payment');
    $this->lock = $lock;
  }

  /**
   * {@inheritdoc}
   */
  public function getPendingOrderIds(): array {
    $orders = &drupal_static(__METHOD__);
    if (!isset($orders)) {
      $orders = [];
      // Get the payment gateways for available plugins.
      $gateways = $this->getPaymentGatewayIds();
      if (!empty($gateways)) {
        $date_range_start = $this->config->get('cron_date_range_start');
        $date_range_end = $this->config->get('cron_date_range_end');

        // Validate the date range are set and valid.
        if (!empty($date_range_start) && !empty($date_range_end)) {
          $date_start = strtotime($date_range_start);
          $date_end = strtotime($date_range_end);
          if ($date_start !== FALSE && $date_end !== FALSE) {

            // Do the query.
            $query = $this->ordersStorage->getQuery();

            // Avoid access check on cron context because it is executed as
            // anonymous user.
            if (!$this->lock->lockMayBeAvailable('cron')) {
              $query->accessCheck(FALSE);
            }

            $query->condition('created', [$date_start, $date_end], 'BETWEEN');
            $conditions = $this->getPendingOrderConditions();
            foreach ($conditions as $field_name => $field_value) {
              if (is_array($field_value)) {
                $operator = 'IN';
              }
              else {
                $operator = '=';
              }
              $query->condition($field_name, $field_value, $operator);
            }
            $result = $query->execute();
            $orders = array_values($result);
          }
        }
      }
    }

    return $orders;
  }

  /**
   * Get the list of payment gateways for available plugins.
   *
   * @return array
   *   The payment gateway ids.
   */
  public function getPaymentGatewayIds(): array {
    return array_keys($this->getPaymentGateways());
  }

  /**
   * Get the collection of payment gateways using.
   *
   * @return array
   *   The array of payment gateway configuration objects.
   */
  protected function getPaymentGateways(): array {
    $payment_gateways = &drupal_static(__METHOD__);
    if (!isset($payment_gateways)) {
      // Load the payment gateways based on the existing commerce_payment_sync
      // plugins.
      $plugin_definitions = $this->pluginManager->getDefinitions();
      if (!empty($plugin_definitions)) {
        $payment_gateways = $this->paymentGatewayStorage
          ->loadByProperties(['plugin' => array_keys($plugin_definitions)]);
      }
      else {
        $payment_gateways = [];
      }
    }
    return $payment_gateways;
  }

  /**
   * Returns a payment gateway.
   *
   * @param string $payment_gateway_id
   *   The payment gateway id.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentGatewayInterface
   *   The payment gateway object.
   *
   * @throws \Drupal\commerce_payment_sync\Exception\PaymentGatewayNotFoundException
   */
  protected function getPaymentGateway(string $payment_gateway_id): PaymentGatewayInterface {
    $payment_gateways = $this->getPaymentGateways();
    if (!isset($payment_gateways[$payment_gateway_id])) {
      throw new PaymentGatewayNotFoundException();
    }
    return $payment_gateways[$payment_gateway_id];
  }

  /**
   * Get the collection of commerce payment sync plugins.
   *
   * @return array
   *   The array of payment sync plugins.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getCommercePaymentSyncPlugins(): array {
    $commerce_payment_sync_plugins = &drupal_static(__METHOD__);
    if (!isset($commerce_payment_sync_plugins)) {
      // Load the payment gateways based on the existing commerce_payment_sync
      // plugins.
      $plugin_definitions = $this->pluginManager->getDefinitions();
      foreach ($plugin_definitions as $id => $definition) {
        $commerce_payment_sync_plugins[$id] = $this->pluginManager->createInstance($id);
      }
    }
    return $commerce_payment_sync_plugins;
  }

  /**
   * Returns a commerce_payment_sync plugin.
   *
   * @param string $plugin_id
   *   The plugin id.
   *
   * @return \Drupal\commerce_payment_sync\CommercePaymentSyncInterface
   *   The commerce payment sync plugin.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getCommercePaymentSyncPlugin(string $plugin_id): CommercePaymentSyncInterface {
    $plugins = $this->getCommercePaymentSyncPlugins();
    if (!isset($plugins[$plugin_id])) {
      throw new PluginNotFoundException($plugin_id);
    }
    return $plugins[$plugin_id];
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentGatewayFromOrder(OrderInterface $order): PaymentGatewayInterface {
    if (!$order->hasField('payment_gateway') || $order->get('payment_gateway')
      ->isEmpty()) {
      throw new WrongOrderException();
    }
    $payment_gateway_id = $order->get('payment_gateway')->getString();
    return $this->getPaymentGateway($payment_gateway_id);
  }

  /**
   * Process an order checking it against gateway API.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to check.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\commerce_payment_sync\Exception\PaymentGatewayNotFoundException
   * @throws \Drupal\commerce_payment_sync\Exception\TransactionNotCompletedException
   * @throws \Drupal\commerce_payment_sync\Exception\TransactionNotFoundException
   * @throws \Drupal\commerce_payment_sync\Exception\WrongOrderException
   */
  public function processOrder(OrderInterface $order): void {
    if (!$this->isOrderPending($order)) {
      throw new WrongOrderException();
    }

    // The commerce payment sync plugin is named equal to the payment gateway
    // plugin.
    $payment_gateway = $this->getPaymentGatewayFromOrder($order);
    $commerce_payment_sync = $this->getCommercePaymentSyncPlugin($payment_gateway->getPluginId());

    // Get the transaction for current order calling payment gateway API.
    $remoteTransaction = $commerce_payment_sync->getRemoteTransaction($order);

    if ($commerce_payment_sync->isTransactionPaid($remoteTransaction)) {
      $payments = $this->paymentStorage->loadByProperties(['order_id' => $order->id()]);
      if (empty($payments)) {
        // Create the payment for the order. The order will be flagged as
        // completed once it gets paid in full.
        $payment = $this->paymentStorage->create([
          'payment_gateway' => $payment_gateway->id(),
          'order_id' => $order->id(),
          'test' => $payment_gateway->getPlugin()->getMode() == 'test',
        ]);
      }
      else {
        // Update the existing payment.
        /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
        $payment = current($payments);
      }

      $commerce_payment_sync->updatePayment($payment, $order, $remoteTransaction);

      if (!$order->get('payment_method')->isEmpty()) {
        /** @var \Drupal\Core\Entity\Plugin\DataType\EntityReference $credit_card */
        $credit_card = $order->get('payment_method')
          ->first()
          ->get('entity')
          ->getTarget()
          ->getValue();
        $payment->set('payment_method', $credit_card);
      }
      $payment->save();
    }
    else {
      throw new TransactionNotCompletedException();
    }

  }

  /**
   * {@inheritdoc}
   */
  public function isOrderPending(OrderInterface $order): bool {
    $conditions = $this->getPendingOrderConditions();

    $failing_conditions = array_filter($conditions, function ($field_value, $field_name) use ($order) {
      // Filter the condition if order fits.
      return ($order->hasField($field_name)
          && !$order->get($field_name)->isEmpty()
          && (
            (is_array($field_value) && in_array($order->get($field_name)
              ->getString(), $field_value))
            || (!is_array($field_value) && $order->get($field_name)
              ->getString() === $field_value)
          )) === FALSE;
    }, ARRAY_FILTER_USE_BOTH);

    // Having an empty array of failing conditions means the order fit all
    // conditions, so the order is pending.
    return empty($failing_conditions);
  }

  /**
   * Get the conditions an order should fit to be considered as pending.
   *
   * @return array
   *   A field name and value pair array of conditions.
   */
  protected function getPendingOrderConditions(): array {
    return [
      'state' => static::PENDING_STATES,
      'checkout_step' => static::CHECKOUT_STEPS,
      'payment_gateway' => $this->getPaymentGatewayIds(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function cancelOrder(OrderInterface $order): void {
    $order->set('state', 'canceled')->save();
  }

}
