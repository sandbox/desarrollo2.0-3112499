<?php

namespace Drupal\commerce_payment_sync\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Commerce Payment Synchronization settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_payment_sync_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['commerce_payment_sync.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_payment_sync.settings');

    $form['cron_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatic synchronization'),
      '#description' => $this->t('Check this option if you want to synchronize the pending payments with the payment gateways automatically by cron.'),
      '#default_value' => $config->get('cron_enabled'),
    ];

    $form['cron_date_range_start'] = [
      '#title' => $this->t('Cron date range start'),
      '#type' => 'select',
      '#options' => [
        '-1 days 00:00:00' => $this->t('Last day'),
        '-2 days 00:00:00' => $this->t('Last 2 days'),
        '-3 days 00:00:00' => $this->t('Last 3 days'),
        '-7 days 00:00:00' => $this->t('Last week.'),
        '-14 days 00:00:00' => $this->t('Last 2 weeks.'),
        '-30 days 00:00:00' => $this->t('Last 30 days.'),
        '-2 months 00:00:00' => $this->t('Last 2 months.'),
      ],
      '#description' => $this->t('The cron will search for commerce orders newer than this date.'),
      '#default_value' => $config->get('cron_date_range_start'),
    ];

    $form['cron_date_range_end'] = [
      '#title' => $this->t('Cron date range end'),
      '#type' => 'select',
      '#options' => [
        '-1 hour' => $this->t('Last hour'),
        '-2 hours' => $this->t('Last 2 hours'),
        '-3 hours' => $this->t('Last 3 hours'),
        '-6 hours' => $this->t('Last 6 hours.'),
      ],
      '#description' => $this->t('The cron will search for commerce orders older than this date.'),
      '#default_value' => $config->get('cron_date_range_end'),
    ];

    $form['cron_limit_attempts'] = [
      '#type' => 'number',
      '#title' => $this->t('Limit of Sermepa payment attempts'),
      '#default_value' => $config->get('cron_limit_attempts'),
      '#size' => 5,
      '#maxlength' => 5,
    ];

    $form['log_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log requests'),
      '#description' => $this->t('Check this option to log all redsys requests.'),
      '#default_value' => $config->get('log_enabled'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('commerce_payment_sync.settings')
      ->set('cron_enabled', $form_state->getValue('cron_enabled'))
      ->set('cron_date_range_start', $form_state->getValue('cron_date_range_start'))
      ->set('cron_date_range_end', $form_state->getValue('cron_date_range_end'))
      ->set('cron_limit_attempts', $form_state->getValue('cron_limit_attempts'))
      ->set('log_enabled', $form_state->getValue('log_enabled'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
