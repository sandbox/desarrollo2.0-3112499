<?php

namespace Drupal\commerce_payment_sync;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;

/**
 * Interface for commerce_payment_sync plugins.
 */
interface CommercePaymentSyncInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  /**
   * Returns a transaction object from a given order entity.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order entity.
   *
   * @return object|null
   *   The transaction object.
   */
  public function getRemoteTransaction(OrderInterface $order): ?object;

  /**
   * Check if a transaction should be considered as paid.
   *
   * @param object $remoteTransaction
   *   The remote transaction retrieved from the payment gateway API.
   *
   * @return bool
   *   boolean indicating if the transaction is paid or not.
   */
  public function isTransactionPaid($remoteTransaction): bool;

  /**
   * Update payment with information retrieved by gateway.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment to update.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order being processed.
   * @param object $remoteTransaction
   *   The remote transaction retrieved from the payment gateway API.
   */
  public function updatePayment(PaymentInterface $payment, OrderInterface $order, $remoteTransaction): void;

  /**
   * Returns a mapping of payment gateway statuses to payment states.
   *
   * @return array
   *   An array containing the payment gateway remote statuses as well as their
   *   corresponding states.
   */
  public function getStatusMapping(): array;

  /**
   * Returns a mapped payment gateway status to a payment state.
   *
   * @param string $remote_status
   *   The payment gateway status.
   *
   * @return string|null
   *   The the corresponding payment state.
   */
  public function getPaymentState(string $remote_status): ?string;

}
