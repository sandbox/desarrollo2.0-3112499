<?php

namespace Drupal\commerce_payment_sync\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines commerce_payment_sync annotation object.
 *
 * @Annotation
 */
class CommercePaymentSync extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
